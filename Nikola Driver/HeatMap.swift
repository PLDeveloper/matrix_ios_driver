//
//  HeatMap.swift
//  Nikola Driver
//
//  Created by Apple on 12/05/18.
//  Copyright © 2018 Sutharshan. All rights reserved.
//

import Foundation
import SwiftyJSON

class HeatMap {
    var latitude: String = "";
    var longitude: String = "";
   
     init(heatMapDet: JSON) {
        
        if heatMapDet["latitude"].exists() {
            latitude = heatMapDet["latitude"].stringValue
        }
        
        if heatMapDet["longitude"].exists() {
            longitude = heatMapDet["longitude"].stringValue
        }
        
    }
    
}
