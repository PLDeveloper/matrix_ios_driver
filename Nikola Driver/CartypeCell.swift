//
//  CartypeCell.swift
//  Nikola Driver
//
//  Created by sudharsan s on 02/12/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//

import UIKit

class CartypeCell: UICollectionViewCell {
    
    @IBOutlet weak var selectedImg: UIImageView!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var carImg: UIImageView!
}
