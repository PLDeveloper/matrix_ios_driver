//
//  PaymentViewController.swift
//  Nikola Driver
//
//  Created by Sutharshan Ram on 25/07/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//

import Foundation
import SwiftyJSON
//import Braintree
//import BraintreeDropIn
import Floaty

class PaymentViewController: BaseViewController, UIGestureRecognizerDelegate, FloatyDelegate, UITableViewDelegate, UITableViewDataSource   {
    
    var countryArray=["Afghanistan","Albania","Algeria","AmericanSamoa","Andorra","Angola","Anguilla","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya"]
    var selectedIndex:IndexPath?
    lazy var isdefaulCardSet:Bool = false
    
    @IBOutlet weak var burgerMenu: UIBarButtonItem!
    
    @IBOutlet weak var addCardView: AddCardView!
    
    @IBOutlet weak var demoUIView: UIView!
    
    var txtViewDate: UITextField!
     var txtViewYear: UITextField!

    var cards:[Card]=[]
    
    var floaty: Floaty? = nil
    var longPressCard: Card? = nil
    let model: Card? = nil
     @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    let defaults = UserDefaults.standard
    
    let expiryDatePicker = MonthYearPickerView()

    //MARK:- Override Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.addCardView?.delegate = self
        
        
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        tap.delegate = self
//        self.addCardView.addGestureRecognizer(tap)
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "BurgerBlack"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btn1.addTarget(self, action: #selector(PaymentViewController.backButton), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navItem.setLeftBarButton(item1, animated: true)
        
        self.navBar.tintColor = UIColor.darkGray
        
        let lpgr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gestureReconizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        lpgr.numberOfTapsRequired = 0
        
        self.tableView.addGestureRecognizer(lpgr)
        self.tableView.tableFooterView = UIView()
        let card:Bool = defaults.bool(forKey: "defaultcard")
            if card {
                isdefaulCardSet = true
            }else {
                isdefaulCardSet = false

           }
        
        layoutFAB()
        self.getAddedCards()
        

        
    }
    
//    func handleTap(sender: UITapGestureRecognizer? = nil)
//    {
//        self
//    }
//
    //MARK:- HandleLongPress
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.began {
            return
        }
        let point = gestureReconizer.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: point)
        
        if let index = indexPath {
            var cell = self.tableView.cellForRow(at: index)
            // do stuff with your cell, for example print the indexPath
            let card: Card = cards[index.row]
            longPressCard = card
            showDeleteCardDialog(card: card)
            print(index.row)
        } else {
            print("Could not find index path")
        }
        
        if isdefaulCardSet {
            let point = gestureReconizer.location(in: self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: point)
            
            if let index = indexPath {
                var cell = self.tableView.cellForRow(at: index)
                // do stuff with your cell, for example print the indexPath
                let card: Card = cards[index.row]
                longPressCard = card
                showDeleteCardDialog(card: card)
                print(index.row)
            } else {
                print("Could not find index path")
            }
        }else {
           
        }
        
        
    }
    
    //MARK:- Navigation Back
    @objc func backButton(sender: Any)
    {
        revealViewController().revealToggle(sender)
    }

    //MARK:- GetAddedCards
    func getAddedCards()
    {
        self.showLoader( str: "Please wait")
        
        API.getAddedCards{ json, error in
            if error != nil {
                print("getAddedCards error")
                print(error!.localizedDescription)
            }else{
                
                if let json = json {
                    
                    debugPrint("json card response \(json)")
                    
                    let status = json[Const.STATUS_CODE].boolValue
                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                    if(status){
                        
                        let jsonArray = json["cards"].arrayValue
                        self.isdefaulCardSet = true
                        self.defaults.set(true, forKey: "defaultcard")
                        
                            self.cards.removeAll()
                            for cardJobj: JSON in jsonArray {
                            self.cards.append(Card.init(jObj: cardJobj))
                        }
                        print(json)
                        self.demoUIView.isHidden = true
                             self.hideLoader()
                            self.tableView.reloadData()
                            print("cards done")
                        }else{
                        
                         self.demoUIView.isHidden = false
                            self.hideLoader()
                    
                            print(statusMessage)
                            print(json)
                    
                     
                        if let error_Code = json[Const.ERROR_CODE].int {
                    
                            if error_Code == 104 {
                    
                                self.showAlert(with: "Session expired")
                    
                            }
                        else {
                    
                                if let msg : String = json[Const.NEW_ERROR].rawString() {
                                    self.showAlert(with: "session expired")
                                }
                            }
                    
                    
                       }
                    
                    
                    
                }
                    
                }else {
                    debugPrint("invalid")
                }
                

        }
        }
        
    }
    
    //MARK:- SetDefaultCard
    func setDefaultCard(card: Card? = nil){
        
        if isdefaulCardSet
        {
            debugPrint("defaultcardsetted")
            API.setDefaultCard(cardNumber: (card?.cardNumber)!, type: (card?.type)!, cardId: (card?.cardId)!, completionHandler: { json, error in
                if error != nil {
                    print("setDefaultCard error")
                    print(error!.localizedDescription)
                }
                else{
                    if let json = json {
                        let status = json[Const.STATUS_CODE].boolValue
                        let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                        if(status){
                            print(json)
                            self.getAddedCards()
                            print("default cards done")
                        }
                        else
                        {
                            print(statusMessage)
                            print(json)
                            var msg = json[Const.DATA].rawString()!
                            self.view.makeToast(message: msg)
                        }
                    }else {
                        debugPrint("Invalid Json :(")
                        
                    }
                    
                    
                }
            })
        }else {
            API.setDefaultCard(cardNumber: "4242 4242 4242 42", type: "visa", cardId: "", completionHandler: { json, error in
                if error != nil {
                    print("setDefaultCard error")
                    print(error!.localizedDescription)
                }else{
                    if let json = json {
                        let status = json[Const.STATUS_CODE].boolValue
                        let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                        if(status){
                            print(json)
                            self.getAddedCards()
                            print("default cards done")
                        }else{
                            print(statusMessage)
                            print(json)
                            if let error_code: Int = json[Const.ERROR_CODE].intValue {
                                if error_code == 101 {
                                    let msg = json[Const.ERROR_MSG].stringValue
                                    self.showAlert(with: msg)
                                }else {
                                    let msg = json[Const.ERROR].stringValue
                                    self.showAlert(with: msg)
                                }
                            }
                        
                        }
                    }else {
                        debugPrint("Invalid Json :(")
                        
                    }
                    
                    
                }
            })
        }
        
        
    }
    
    //MARK:- RemoveCard
    func removeCard(card: Card){
        print(card.cardId)
        
        API.removeCard(cardId: card.cardId, completionHandler: { json, error in
            if error != nil {
                print("removeCard error")
                print(error!.localizedDescription)
            }else{
                
                if let json = json {
                        let status = json[Const.STATUS_CODE].boolValue
                        let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                        if(status){
                            print(json )
                            self.getAddedCards()
                            print("default cards done")
                        }else{
                            print(statusMessage)
                            print(json ?? "json empty")
                                        var msg = ""
                            if json[Const.ERROR].exists() {
                                msg = json[Const.ERROR].stringValue
                            }else if json[Const.MESSAGE].exists() {
                                msg = json[Const.MESSAGE].stringValue
                            }
                            if msg.isEmpty {
                                msg = "Unable to proceed with action. Please try again"
                            }
                            self.view.makeToast(message: msg, duration: 3.0, position: "center" as AnyObject)
                                        //self.view.makeToast(message: msg)
                        }
                    
                }else {
                    debugPrint("Invalid Json :(")
                    
                }
   
            }
        })
    }

    

    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         if isdefaulCardSet {
            return cards.count
            
         }else {
           // return 1
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PaymentCell=tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        
        if isdefaulCardSet
        {
            
            let card: Card = cards[indexPath.row]
            
            cell.cardNumber.text = "XXXX XXXX XXXX \(card.cardNumber)"
            
            if (selectedIndex == indexPath || card.isDefault == "1") {
                cell.radioButton.setImage(UIImage(named: "radioButtonChecked"),for:.normal)
            } else {
                cell.radioButton.setImage(UIImage(named: "radioButtonUnchecked"),for:.normal)
            }
            
            let cardType: String = card.cardtype.lowercased()
            
            var tempCardImg: UIImage? = nil
            if card.type == "card" {
                switch (cardType) {
                case "americanexpress":
                    tempCardImg = UIImage(named: "bt_amex")
                    break;
                case "visa":
                    tempCardImg = UIImage(named: "bt_visa")
                    break;
                case "mastercard":
                    tempCardImg = UIImage(named: "bt_mastercard")
                    break;
                case "jcb":
                    tempCardImg = UIImage(named: "bt_jcb")
                    break;
                case "maestro":
                    tempCardImg = UIImage(named: "bt_maestro")
                    break;
                case "dinersclub":
                    tempCardImg = UIImage(named: "bt_diners")
                    break;
                case "chinaunionPay":
                    tempCardImg = UIImage(named: "bt_card_highlighted")
                    break;
                default:
                    tempCardImg = UIImage(named: "bt_card_highlighted")
                }
                cell.radioButton.isHidden = false
            }
                
            else{
                tempCardImg = UIImage(named: "ub__creditcard_paypal")
                cell.cardNumber.text = "\(card.email)"
                cell.radioButton.isHidden = true
            }
            
            cell.cardTypeImg.image = tempCardImg
            
        }
        else
        {
             cell.cardNumber.text = "XXXX XXXX XXXX 4242"
             cell.cardTypeImg.image = UIImage(named: "bt_visa")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let row = indexPath.row
        print(countryArray[row])
        selectedIndex = indexPath
        self.setDefaultCard(card: cards[row])
        tableView.reloadData()
    }
    
    func showDeleteCardDialog(card: Card)
    {
        let alert = UIAlertController(title: "", message: "Are you sure you want to remove this card?", preferredStyle: .alert)
        let confirmAction = UIAlertAction(
        title: "Yes", style: UIAlertAction.Style.default) { (action) in
            self.removeCard(card: card)
        }
        alert.addAction(confirmAction)
        let cancelAction = UIAlertAction(
        title: "No", style: UIAlertAction.Style.default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- FABButton
    func layoutFAB()
    {
        
        floaty = Floaty()
        floaty?.buttonColor = UIColor(red:1.00, green:0.59, blue:0.00, alpha:1.0)
        floaty?.buttonImage = UIImage(named: "add-credit-card")
        
        self.view.addSubview(floaty!)
        floaty?.fabDelegate = self
        
    }
    
    func emptyFloatySelected(_ floaty: Floaty)
    {
        print("Floaty emptyFloatySelected")
        self.addCardView.txtCardNo.text = ""
        self.addCardView.txtDate.text = ""
        self.addCardView.txtYear.text = ""
        self.addCardView.txtCVV.text = ""
        self.addCardView.isHidden = false
      //  getBrainTreeClientToken()
    }
    
    func floatyOpened(_ floaty: Floaty) {
        print("Floaty Opened")
    }
    
    func floatyClosed(_ floaty: Floaty) {
        print("Floaty Closed")
    }

    //MARK:- BrainTreeClientToken
    func getBrainTreeClientToken(){
    }
    
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
    }

    func sendNonce(nonce: String){
        API.sendNonce(nonce: nonce, completionHandler: { json, error in
            if (error != nil) {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json {
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status){
                    self.view.makeToast(message: "Card added successfully")
                    self.getAddedCards()
                        print(json ?? "error in sendNonce json")
                                    //self.goToDashboard()
                    }
                else
                {
                        print(statusMessage)
                        print(json ?? "sendNonce json empty")
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                         self.view.makeToast(message: msg)
                    }
                                   // var msg = json[Const.ERROR].rawString()!
                    
                }
                
            }else {
                debugPrint("Invalid Json :(")
            }

        })
    }
    
}


//MARK:- UIImage Extention
extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}



extension PaymentViewController : UITextFieldDelegate
{
    func shouldChangeText(in range: UITextRange, replacementText text: String) -> Bool
    {
        print("Values --> \(text)")
        print(text)
        return true
        
    }
}

extension PaymentViewController: AddCardViewDelegate
{
    func submitCardAction(cardNumber: String, month: String, year: String, cvv: String)
    {
       print("Enter success")
        
        if  cardNumber == "" || month == "" || year == "" || cvv == ""
        {
            self.view.makeToast(message: "All fields are mandatory")
        }
        else
        {
        
            self.showLoader( str: "Please wait")
            
            API.addCard(cardNo:cardNumber, cardCVC: cvv, cardExpiryMonth: month, cardExpiryYear: year, completionHandler: { json, error in
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    self.view.makeToast(message: json[Const.MESSAGE].stringValue)
                    self.getAddedCards()
                    self.addCardView.isHidden = true
                    print(json ?? "error in sendNonce json")
                    //self.goToDashboard()
                }
                else
                {
                    
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                    
                    if let msg : String = json[Const.ERROR].rawString()
                    {
                        self.view.makeToast(message: msg)
                    }
                    // var msg = json[Const.ERROR].rawString()!
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
            
        })
    }
    }
    
    func cancelAction() {
        self.addCardView.isHidden = true
    }
    
}
