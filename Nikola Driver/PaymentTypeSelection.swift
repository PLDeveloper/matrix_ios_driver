//
//  PaymentTypeSelection.swift
//  Wakuk
//
//  Created by Jeya R on 5/9/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

protocol PaymentTypeSelectionDelegate
{
    func payByCardAction()
    func payByBankAction()
    func cancelPopup()
}

class PaymentTypeSelection: UIView
{
    
    @IBOutlet var viewPaymentTypeSelection: UIView!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var btnPayByCard: UIButton!
    @IBOutlet weak var btnPayByBank: UIButton!
    
    var delegate: PaymentTypeSelectionDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("PaymentTypeSelection", owner: self, options: nil)
        self.addSubview(self.viewPaymentTypeSelection!)
        initializeView()
        self.layoutIfNeeded()
    }
    
    func initializeView()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        self.viewPaymentTypeSelection.addGestureRecognizer(tap)

    }
    
    @objc func handleTap()
    {
        self.delegate?.cancelPopup()
    }
    
    @IBAction func tapOnPayByCard(_ sender: Any)
    {
        self.delegate?.payByCardAction()
    }
    
    @IBAction func tapOnPayByBank(_ sender: Any)
    {
        self.delegate?.payByBankAction()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
