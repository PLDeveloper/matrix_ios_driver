//
//  BaseVC.swift
//  Nikola Driver
//
//  Created by Shantha Kumar on 9/28/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//

import Foundation


class BaseViewController: UIViewController {

    var jsonParser = JsonParser()
    public var hud : MBProgressHUD = MBProgressHUD()

}
extension BaseViewController: MBProgressHUDDelegate {
    
    public func showAlertMessage(with alertMessage: String) {
        let alert = UIAlertController(title: "Message", message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoader(str: String) {
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = MBProgressHUDModeIndeterminate
        hud.labelText = str
    }
    
    func hideLoader() {
        hud.hide(true)
    }
    
    
    
}
