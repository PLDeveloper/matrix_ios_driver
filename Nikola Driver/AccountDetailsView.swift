//
//  AccountDetailsView.swift
//  Wakuk
//
//  Created by Jeya R on 5/9/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

protocol AccountDetailsViewDelegate
{
    func submitAccountDetails(accountNumber:String, code:String)
    func cancelAccountDetailsPopup()
}

class AccountDetailsView: UIView
{
    
    @IBOutlet var accountDetailsView: UIView!
    
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtAccountNo: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var setVerticalConstraints: NSLayoutConstraint!
    var delegate: AccountDetailsViewDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("AccountDetailsView", owner: self, options: nil)
        self.addSubview(self.accountDetailsView!)
        initializeView()
        self.layoutIfNeeded()
    }
    
    func initializeView()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.accountDetailsView.addGestureRecognizer(tap)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        txtCode.leftView = paddingView
        txtCode.leftViewMode = .always
        
        let paddingViewTxtCVV = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        txtAccountNo.leftView = paddingViewTxtCVV
        txtAccountNo.leftViewMode = .always
        
        txtAccountNo.layer.cornerRadius = Const.CORNER_RADIUS
        txtAccountNo.layer.borderColor = UIColor.lightGray.cgColor
        txtAccountNo.layer.borderWidth = 1
        
        txtCode.layer.cornerRadius = Const.CORNER_RADIUS
        txtCode.layer.borderColor = UIColor.lightGray.cgColor
        txtCode.layer.borderWidth = 1
        
        btnSubmit.layer.cornerRadius = Const.CORNER_RADIUS
        btnCancel.layer.cornerRadius = Const.CORNER_RADIUS
        
        // Add keyboard done button
        
        SetDoneToolbar(field: txtAccountNo)
        SetDoneToolbar(field: txtCode)
        
    }
    
    @objc func handleTap()
    {
        self.delegate?.cancelAccountDetailsPopup()
    }
    
    func SetDoneToolbar(field:UITextField)
    {
        let doneToolbar:UIToolbar = UIToolbar()
        
        doneToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(keyboardCancel))
        ]
        
        doneToolbar.sizeToFit()
        field.inputAccessoryView = doneToolbar
    }
    
    @objc func keyboardCancel()
    {
        
        txtAccountNo.resignFirstResponder()
        txtCode.resignFirstResponder()
//        self.changeYConstraints.constant = 0
    }

    @IBAction func tapOnCancelDetails(_ sender: Any)
    {
        self.delegate?.cancelAccountDetailsPopup()
    }
    
    @IBAction func tapOnSubmitDetails(_ sender: Any)
    {
        self.accountDetailsView.endEditing(true)
        self.delegate?.submitAccountDetails(accountNumber: txtAccountNo.text!, code: txtCode.text!)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


extension AccountDetailsView : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.setVerticalConstraints.constant = 0
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.setVerticalConstraints.constant = -90.0
        return true
    }
    
}
