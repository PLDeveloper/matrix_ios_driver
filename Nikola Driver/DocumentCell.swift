//
//  DocumentCell.swift
//  Nikola Driver
//
//  Created by Sutharshan Ram on 21/08/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//


import Foundation

import UIKit

class DocumentCell: UITableViewCell {
    
    @IBOutlet weak var docName: UILabel!
    @IBOutlet weak var docImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
}

