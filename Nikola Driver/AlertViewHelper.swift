//
//  AlertViewHelper.swift
//  Enjoy Apps
//
//  Created by Shantha Kumar on 7/27/17.
//  Copyright © 2017 Shantha Kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert1(with messgage:String,leftButtonTitle:String?=nil,rightButtonTitle:String?=nil) {
        let alert = UIAlertController(title: "Message", message: messgage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    
    
    
}

