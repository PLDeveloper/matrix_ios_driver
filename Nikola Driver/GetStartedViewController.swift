//
//  GetStartedViewController
//  Nikola
//
//  Created by Sutharshan on 5/22/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//

import UIKit
import Localize_Swift



class GetStartedViewController: UIViewController {
    
    
    var actionSheet: UIAlertController!
    let availableLanguages = Localize.availableLanguages()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func getStartedBtn(_ sender: UIButton) {
        let user_defaults = UserDefaults.standard
        let userId = user_defaults.string(forKey: Const.Params.ID)
        self.performSegue(withIdentifier: "loginScreen", sender: nil)
        
        //let mobileVerified = user_defaults.string(forKey: Const.MOBILE_VERIFIED)
        
//        if (userId ?? "").isEmpty  {
//            goToSignIn()
//        }else{
//            goToDashboard()
//        }        
    }
    func goToSignIn(){        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewContro‌​ller = storyBoard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(nextViewContro‌​ller, animated: true)
        
    }
    
    
    
    @IBAction func changeLanguageButtonAction(_ sender: Any) {
        
        actionSheet = UIAlertController(title: nil, message: "Switch Language", preferredStyle: UIAlertController.Style.actionSheet)
            for language in availableLanguages {
                let displayName = Localize.displayNameForLanguage(language)
                if language == "Base" {
                    
                }else {
                    let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        Localize.setCurrentLanguage(language)
                    })
                    actionSheet.addAction(languageAction)
                }
                
            }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
                (alert: UIAlertAction) -> Void in
            })
            actionSheet.addAction(cancelAction)
            self.present(actionSheet, animated: true, completion: nil)
            
    }
    

    func goToDashboard(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(secondViewController, animated: true, completion: nil)
    }
}

