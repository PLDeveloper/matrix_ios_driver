//
//  Extension.swift
//  Nikola Driver
//
//  Created by Shantha Kumar on 11/01/18.
//  Copyright © 2018 Sutharshan. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
    }
    
}

extension UILabel {

    func setMultipleColorToFont(_ text: String)  {
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font:UIFont(name: "SourceSansPro-Semibold", size: 17.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:2,length:4))
        // set label Attribute
        self.attributedText = myMutableString
    }
}
