//
//  PayByBankViewController.swift
//  Wakuk
//
//  Created by Jeya R on 5/9/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

class PayByBankViewController: BaseViewController
{
    
    @IBOutlet weak var tblViewBankList: UITableView!
    
    @IBOutlet weak var accountDetailsView: AccountDetailsView!
    
    @IBOutlet weak var viewUserInput: userInputWindow!
    var arrBankName = [String]()
    
    var walletAmount:String = ""
    var userInputURL:String = ""
    var userInputReferenceId:String = ""
    
    //var arrBank:NSMutableArray = [String]()

 //   var swiftArray = nsArray as AnyObject as [String]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "AVAILABLE BANKS"
        
        self.accountDetailsView?.delegate = self
        self.viewUserInput?.delegate = self
        
        getBankName()
        
    }
    
    func getBankName()
    {
        
        self.showLoader(str: "Getting Bank Details...")

        API.bankDetailsAPICall(with: Const.Url.PAYSTACK_BANK_URL) { responseObject, error in

            //                    print(responseObject!)
            
            self.hideLoader()
            
            if let error = error
            {
                self.showAlert(with: error.localizedDescription)
                debugPrint("Error occuring while fetching provider.service :( | \(error.localizedDescription)")
                return
            }

//            if responseObject == nil {
//                print("object nil")
//                return
//            }

            let json = self.jsonParser.jsonParser(dicData: responseObject)
            
            if let arrAddress = json["data"].array {
                
              //  self.walletPaymentGatewayTypeArray.removeAll()
                
                for i in 0..<arrAddress.count
                {
                    
                    let dicAddr = arrAddress[i].dictionary
                    
                    let fullAddr : String = (dicAddr!["name"]?.string)! + "/" + (dicAddr!["code"]?.string)!
                    
                    print(fullAddr)
                    
                    
                  // self.arrBank.addObjects(from: fullAddr)
                    
                    self.arrBankName.append(fullAddr)
                    
                    print(self.arrBankName)
                    
                  //  self.walletPaymentGatewayTypeArray.append(fullAddr)
                }
                
                self.tblViewBankList.reloadData()
            
//            let gateways = json["data"].dictionary
//
//            let array = (gateways!["gateways"]?.array)!

            
            print(json)

        }
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PayByBankViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.view.frame.size.height / 12
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return gatewayNameArray.count
        return self.arrBankName.count

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.accountDetailsView.isHidden = false
        self.accountDetailsView.txtAccountNo.text = ""
        self.accountDetailsView.txtCode.text = self.arrBankName[indexPath.row].components(separatedBy: "/")[1]
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: BankListCell = tableView.dequeueReusableCell(withIdentifier: "BankListCell", for: indexPath) as! BankListCell
        cell.lblBankName.text = self.arrBankName[indexPath.row].components(separatedBy: "/")[0]
        cell.lblBankName.layer.cornerRadius = 3.0
        cell.lblBankName.layer.borderColor = UIColor.black.cgColor
        cell.lblBankName.layer.borderWidth = 1
        
        return cell
    }
    
    
}

extension PayByBankViewController:userInputWindowDelegate
{
    
    func hidePopup()
    {
        self.viewUserInput?.isHidden = true
    }
    
    func inputSubmitAction(userInput:String)
    {
        if (userInput == "")
        {
            self.showAlert(with: "All fields are mandatory")
            return
        }
        
        self.showLoader(str: "Process Payment...")
        API.verifyUserInput(inputURL: self.userInputURL, referenceId: self.userInputReferenceId, authInput: userInput, payBy: "wallet", completionHandler: { json, error in
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
               
                
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    
                    
                    self.accountDetailsView.isHidden = true
                    self.accountDetailsView.isHidden = true
                    
                    if json["status"].string == Const.STATUS_CODE
                    {
                        self.view.makeToast(message: "Amount added to wallet")
                        self.navigationController?.popViewController(animated: true)
                        self.viewUserInput?.isHidden = true
                        return
                    }
                    else if json["status"].string == Const.STATUS_FAILED
                    {
                        self.view.makeToast(message: "Payment Failed! Please Try again")
                        return
                    }
                    else
                    {
                        
                    }
                   
                    let compareStr = json[Const.STATUS].string
                    self.userInputURL = json[Const.URL_POST_DATA].string!
                    self.userInputReferenceId = json[Const.Params.REFERENCE_ID].string!
                    
                    print(self.userInputURL)
                    print(self.userInputReferenceId)
                    
                    self.viewUserInput?.txtInputField.text = ""
                    self.viewUserInput?.txtInputField.keyboardType = UIKeyboardType.numberPad

                    if compareStr == "send_pin"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter PIN"
                    }
                    else if compareStr == "send_phone"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter Phone number"
                    }
                    else if compareStr == "send_otp"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter OTP"
                    }
                    else if compareStr == "send_birthday"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.keyboardType = UIKeyboardType.numbersAndPunctuation

                        self.viewUserInput?.txtInputField.placeholder = "Enter Birthday - YYYY-MM-DD"
                    }
                    else
                    {
                        self.accountDetailsView.isHidden = true
                        self.view.makeToast(message: statusMessage)
                    }
                    
                    print(json ?? "error in sendNonce json")
                }
                else
                {
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                    self.accountDetailsView.isHidden = true
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                        self.view.makeToast(message: msg)
                    }
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
            
        })
    }

}

extension PayByBankViewController: AccountDetailsViewDelegate
{
    func submitAccountDetails(accountNumber: String, code: String)
    {
        
        if (accountNumber == "" || code == "")
        {
            self.showAlert(with: "All fields are mandatory")
            return
        }
        
        self.showLoader(str: "Process Payment...")

        API.addAmountToWallet(paymentMode: "paystack", amount: walletAmount,bankCode: code,bankAccNumber:accountNumber, completionHandler: { json, error in
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    
                    self.accountDetailsView.isHidden = true

                    if json["status"].string == Const.STATUS_CODE
                    {
                        self.view.makeToast(message: "Amount added to wallet")
                        return
                    }
                    else if json["status"].string == Const.STATUS_FAILED
                    {
                        self.view.makeToast(message: "Payment Failed! Please Try again")
                        return
                    }
                    else
                    {
                        
                    }
                    
                    let compareStr = json[Const.STATUS].string
                    self.userInputURL = json[Const.URL_POST_DATA].string!
                    self.userInputReferenceId = json[Const.Params.REFERENCE_ID].string!
                    
                    print(self.userInputURL)
                    print(self.userInputReferenceId)
                    
                    self.viewUserInput?.txtInputField.text = ""
                    self.viewUserInput?.txtInputField.keyboardType = UIKeyboardType.numberPad

                    if compareStr == "send_pin"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter PIN"
                    }
                    else if compareStr == "send_phone"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter Phone number"
                    }
                    else if compareStr == "send_otp"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.placeholder = "Enter OTP"
                    }
                    else if compareStr == "send_birthday"
                    {
                        self.viewUserInput?.isHidden = false
                        self.viewUserInput?.txtInputField.keyboardType = UIKeyboardType.numbersAndPunctuation

                        self.viewUserInput?.txtInputField.placeholder = "Enter Birthday - YYYY-MM-DD"
                    }
                    else
                    {
                        self.accountDetailsView?.isHidden = true
                        self.view.makeToast(message: statusMessage)
                    }
                    
                    print(json ?? "error in sendNonce json")
                    
                }
                else
                {
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                    self.accountDetailsView.isHidden = true
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                        if let message : String = json["error_messages"].rawString()
                        {
                          self.view.makeToast(message: message)
                        }
                        else
                        {
                        self.view.makeToast(message: msg)
                        }
                    }
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
        })
    }
    
    func cancelAccountDetailsPopup()
    {
            self.accountDetailsView.isHidden = true
            self.accountDetailsView.txtAccountNo.text = ""
            self.accountDetailsView.txtCode.text = ""
    }
    
}
