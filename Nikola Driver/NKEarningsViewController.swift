//
//  NKEarningsViewController.swift
//  Yiiri
//
//  Created by Vinitha on 31/10/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

class NKEarningsViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtReferralCode: UITextField!
    
    @IBOutlet weak var BtnApply: UIButton!
    
    @IBOutlet weak var lblEarningamount: UILabel!
    
    
    @IBOutlet weak var lblReferralCode: UILabel!
    
     var hud : MBProgressHUD = MBProgressHUD()
    
     var isAlreadyApplied = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        callGetRefferalCode()
        
        txtReferralCode.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleKeyboardDidShowNotification(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleKeyboardDidHideNotification(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        
        
    }
    
    @objc func handleKeyboardDidShowNotification(_ notification: Notification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
            self.view.frame.origin.y -= keyboardSize.height
            UIView.animate(withDuration: duration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    @objc func handleKeyboardDidHideNotification(_ notification: Notification) {
        
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtReferralCode.resignFirstResponder()
        return true
    }

    func  setupUI()
    {
        BtnApply.layer.cornerRadius = 25
        BtnApply.clipsToBounds = true
        txtReferralCode.layer.cornerRadius = 25
        txtReferralCode.clipsToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @IBAction func btnBAckPressed(_ sender: Any) {
    goToDashboard()
}

func goToDashboard(){
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let secondViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as? UIViewController
    self.present(secondViewController!, animated: true, completion: nil)
}

    
    func callGetRefferalCode()
    {
        
        API.getReferralCode{ json, error in
            
            if let error = error {
                self.showAlert(with :error.localizedDescription)
                //print(error.debugDescription)
                return
            }else {
                if let json = json {
                    let status = json[Const.STATUS_CODE].boolValue
                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                    if(status){
                      
                    
                      
                        print(json ?? "error in airports json")
                        self.isAlreadyApplied = json["already_applied"].boolValue
                        self.lblReferralCode.text = json["referral_code"].stringValue
                        self.lblEarningamount.text = json["referral_earnings"].stringValue
                        
                        
                        //setLatLonFromAddress
                        
                    }else{
                        print(statusMessage)
                        print(json ?? "json empty")
                        var msg = json[Const.ERROR].rawString()!
                        
                        self.view.makeToast(message: msg)
                    }
                }else {
                    print("invalid json :(")
                }
                
                
            }
            
            
        }
        
    }
    @IBAction func btnApplyPressed(_ sender: Any) {
        self.view.endEditing(true)
       if(txtReferralCode.text == "")
       {
        self.view.makeToast(message: "Please enter Referral Code.")
       }
        else if(isAlreadyApplied)
       {
            self.view.makeToast(message: "Referral can be applied only once.")
        }
        else
       {
          callApplyReferalcode()
        }
    }
    
    
   func callApplyReferalcode()
    {
        
    API.postReferralCode(referalCode: txtReferralCode.text!){ json, error in
    
    if (error != nil) {
    print(error.debugDescription)
    self.hideLoader()
    self.view.makeToast(message: (error?.localizedDescription)!)
    }else{
    do{
    let status = json![Const.STATUS_CODE].boolValue
    let statusMessage = json![Const.STATUS_MESSAGE].stringValue
    if(status){
    self.hideLoader()
   
    print(json ?? "json null")
        self.isAlreadyApplied = json!["already_applied"].boolValue
       self.view.makeToast(message: "Applied Successfully.")
       
    
    }else{
    self.hideLoader()
    print(statusMessage)
    print(json ?? "json empty")
    var msg = try json!["error"].stringValue
    
    self.view.makeToast(message: msg)
    }
    }catch {
    self.hideLoader()
    self.view.makeToast(message: "Server Error")
    print("json error")
    }
    }
    }
    
    
    }
    

}


extension NKEarningsViewController : MBProgressHUDDelegate {
    
    func showLoader(str: String) {
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = MBProgressHUDModeIndeterminate
        hud.labelText = str
    }
    
    func hideLoader() {
        hud.hide(true)
    }
    
    
}

