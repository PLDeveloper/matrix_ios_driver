//
//  BankListCell.swift
//  Wakuk
//
//  Created by Jeya R on 5/9/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

class BankListCell: UITableViewCell {

    
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var lblBankName: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
