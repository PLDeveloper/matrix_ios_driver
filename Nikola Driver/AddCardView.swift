//
//  AddCardView.swift
//  Wakuk
//
//  Created by Jeya R on 5/7/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

protocol AddCardViewDelegate
{
    func submitCardAction(cardNumber: String, month: String, year: String, cvv: String)
    func cancelAction()
    
}

class AddCardView: UIView
{
    
    @IBOutlet private var contentView:UIView?
    
    @IBOutlet weak var viewDateYear: UIView!
    
    @IBOutlet weak var viewDatePicker: UIView!
    
    @IBOutlet weak var addCardView: UIView!

    @IBOutlet weak var popupView: UIView!

    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var txtCardNo: UITextField!

    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var changeYConstraints: NSLayoutConstraint!

    var delegate: AddCardViewDelegate?

    let expiryDatePicker = MonthYearPickerView()
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("AddCardView", owner: self, options: nil)
        self.addSubview(self.addCardView!)
        initializeView()
        self.layoutIfNeeded()
    }
    
    func initializeView()
    {
        viewDateYear?.layer.cornerRadius = Const.CORNER_RADIUS
        viewDateYear?.clipsToBounds =  true
        viewDateYear?.layer.borderWidth = 1
        viewDateYear?.layer.borderColor = UIColor.lightGray.cgColor
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.txtCardNo.leftView = paddingView
        self.txtCardNo.leftViewMode = .always
        
        let paddingViewTxtCVV = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.txtCVV.leftView = paddingViewTxtCVV
        self.txtCVV.leftViewMode = .always
        
        let paddingViewTxtMonth = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.txtDate.leftView = paddingViewTxtMonth
        self.txtDate.leftViewMode = .always
        
        let paddingViewTxtYear = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.txtYear.leftView = paddingViewTxtYear
        self.txtYear.leftViewMode = .always

        txtCardNo.layer.cornerRadius = Const.CORNER_RADIUS
        txtCardNo.layer.borderColor = UIColor.lightGray.cgColor
        txtCardNo.layer.borderWidth = 1
        
        txtCVV.layer.cornerRadius = Const.CORNER_RADIUS
        txtCVV.layer.borderColor = UIColor.lightGray.cgColor
        txtCVV.layer.borderWidth = 1
        
        btnSubmit.layer.cornerRadius = Const.CORNER_RADIUS
        btnCancel.layer.cornerRadius = Const.CORNER_RADIUS

        // Add keyboard done button
        
        SetDoneToolbar(field: self.txtCardNo)
        SetDoneToolbar(field: self.txtCVV)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        self.addCardView.addGestureRecognizer(tap)

    }
    
    @objc func handleTap()
    {
       // self.delegate?.cancelAction()
    }
    
    func SetDoneToolbar(field:UITextField)
    {
        let doneToolbar:UIToolbar = UIToolbar()
        
        doneToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(keyboardCancel))
        ]
        
        doneToolbar.sizeToFit()
        field.inputAccessoryView = doneToolbar
    }
    
    @objc func keyboardCancel()
    {
        
        self.txtCardNo.resignFirstResponder()
        self.txtDate.resignFirstResponder()
        self.txtYear.resignFirstResponder()
        self.txtCVV.resignFirstResponder()

        self.changeYConstraints.constant = 0
    }
    
    func keyboardApply()
    {
        
    }
    
    @IBAction func tapOnSubmit(_ sender: Any)
    {
        self.addCardView.endEditing(true)
        self.changeYConstraints.constant = 0
        self.delegate?.submitCardAction(cardNumber: self.txtCardNo.text!, month: self.txtDate.text!, year: self.txtYear.text!, cvv: self.txtCVV.text!)
    }
    
    @IBAction func tapOnCancel(_ sender: Any)
    {
        self.delegate?.cancelAction()
    }
}

    extension AddCardView : UITextFieldDelegate
    {
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
        {
            
            if textField == self.txtCardNo {
                if (self.txtCardNo.text?.length)! <= 40
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else if textField == self.txtCVV
            {
                if (self.txtCVV.text?.length)! <= 8
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
            return true
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool
        {
            self.changeYConstraints.constant = 0
            return true
        }
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
        {

            if textField == self.txtYear || textField == self.txtDate
            {
                self.addCardView.endEditing(true)
                expiryDatePicker.removeFromSuperview()
                expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
                  //  let string = String(format: "%02d/%d", month, year)
                    if String(month).length > 1
                    {
                        self.txtDate.text = String(month)
                    }
                    else
                    {
                        self.txtDate.text =   "0" + String(month)
                    }
                    
                    self.txtYear.text = String(year)
  
                    self.expiryDatePicker.removeFromSuperview()
                   // NSLog(string) // should show something like 05/2015
                }
                expiryDatePicker.frame = CGRectMake(0, 0, self.frame.size.width, self.viewDatePicker.frame.size.height)
                self.viewDatePicker.addSubview(expiryDatePicker)
                return false
            }
            self.changeYConstraints.constant = -80.0
            return true
        }
        
    }



extension UITextField
{
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
        {
            return true
        }
}


