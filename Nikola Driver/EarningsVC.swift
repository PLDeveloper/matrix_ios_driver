//
//  EarningsVC.swift
//  Nikola Driver
//
//  Created by Shantha Kumar on 11/01/18.
//  Copyright © 2018 Sutharshan. All rights reserved.
//

import UIKit
import Charts

class EarningsVC: BaseViewController {

    //MARK: varVars
//     var hud : MBProgressHUD = MBProgressHUD()

    @IBOutlet weak var burgerMenu: UIBarButtonItem!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var totalEarningView: UIView!
    @IBOutlet weak var todayEarningView: UIView!
    @IBOutlet weak var todaysEarningTotalLabel: UILabel!
    @IBOutlet weak var todayEarningTripLabel: UILabel!
    @IBOutlet weak var totalEarningsLabel: UILabel!


    private var earningsModel: EarningsModel?
    private var weeks: [String] = []
    private var barElements: [Double] = []

    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        setUpRevealViewController()
       // self.setupNavigationBar(with: "Sign Up")
        getEarningsAPI()
        // Do any additional setup after loading the view.
    }

    //MARK:- RevealViewController Setup
    private func setUpRevealViewController() {
        if revealViewController() != nil {

            burgerMenu.target = revealViewController()
            burgerMenu.action = #selector(SWRevealViewController.revealToggle(_:))

            self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        }

    }

    //MARK:- UIElements Setting
    private func setUpElements() {
        self.title = "Tip Earnings"
        totalEarningView.dropShadow()
        todayEarningView.dropShadow()

    }


    //MARK:- EarningsAPICall
    private func getEarningsAPI() {

        self.showLoader(str: "Please wait..")
        API.getProvdierEarningsAPI() { [weak self] json, error in
            if let error = error {
                self?.hideLoader()
                self?.showAlertMessage(with: error.localizedDescription)
                debugPrint(error.localizedDescription)
            }else {
                if let json = json {
                    let result = json["success"] as! Bool

                    if result {
                        self?.hideLoader()
                        self?.earningsModel = EarningsModel.init(responseDic: json)

                        let totalEarning: String = (self?.earningsModel?.totalEarnings) ?? ""
                        let currency : String = (self?.earningsModel?.currency)!

                       // self?.totalEarningsLabel.text  = "Total Earnings : $ \(totalEarning)"
                        self?.totalEarningsLabel.text  = "Total Tip Earnings : \(currency) \(totalEarning)"

                        let total: Double = (self?.earningsModel?.earningsMeta.first?.total) ?? 0.0


                       // self?.todaysEarningTotalLabel.text = "$ \(total)"
                         self?.todaysEarningTotalLabel.text = "\(currency) \(total)"

                        let trip: String = (self?.earningsModel?.earningsMeta.first?.trips) ?? "0"
                        self?.todayEarningTripLabel.text = "\(trip) Trips"

                        self?.earningsModel?.earningsMeta.remove(at: 0)

                        self?.earningsModel?.earningsMeta.forEach { earng in

                            self?.weeks.append(earng.day)
                            self?.barElements.append(earng.total)
                        }

                        self?.barChartView.setBarChartData(xValues: (self?.weeks)!, yValues: (self?.barElements)!, label: "This Week Earnings")
                        self?.barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
                       // self?.barChartView.description = ""

                    }else {
                        self?.showAlertMessage(with: "Something went worng")
                    }

                    print(json)
                }else {
                    self?.hideLoader()
                    self?.showAlertMessage(with: "Something went worng")
                    debugPrint("Invalid Json")
                }
            }
        }
    }

}
extension BarChartView {

    private class BarChartFormatter: NSObject, IAxisValueFormatter {

        var labels: [String] = []

        func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return labels[Int(value)]
        }

        init(labels: [String]) {
            super.init()
            self.labels = labels
        }
    }

    func setBarChartData(xValues: [String], yValues: [Double], label: String) {

        var dataEntries: [BarChartDataEntry] = []

        for i in 0..<yValues.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: yValues[i])
            dataEntries.append(dataEntry)
        }

        let chartDataSet = BarChartDataSet(entries: dataEntries, label: label)
        chartDataSet.colors = ChartColorTemplates.colorful()
        let chartData = BarChartData(dataSet: chartDataSet)

        let chartFormatter = BarChartFormatter(labels: xValues)
        let xAxis = XAxis()
        xAxis.valueFormatter = chartFormatter
        self.xAxis.valueFormatter = xAxis.valueFormatter

        self.data = chartData
    }
}


//extension EarningsVC : MBProgressHUDDelegate {
//
//    func showLoader(str: String) {
//        hud = MBProgressHUD.showAdded(to: view, animated: true)
//        hud.mode = MBProgressHUDModeIndeterminate
//        hud.labelText = str
//    }
//
//    func hideLoader() {
//        hud.hide(true)
//    }
//
//
//}



