//
//  WalletTableViewCell.swift
//  eRide Driver
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Sutharshan. All rights reserved.
//

import UIKit
import Localize_Swift

class WalletTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltittle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lbltittle.text = "Wallet".localized()
        // Configure the view for the selected state
    }
    
}
