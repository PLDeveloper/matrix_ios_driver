//
//  Model.swift
//  Nikola Driver
//
//  Created by Shantha Kumar on 11/01/18.
//  Copyright © 2018 Sutharshan. All rights reserved.
//

import Foundation


struct EarningsModel {
    
    
    let totalEarnings: String
    let currency: String
    var earningsMeta: [EarningsMeta]
    
    
    init(responseDic: [String: Any]) {
      
        self.totalEarnings = "\(responseDic["total_earnings"]!)"
        self.currency = "\(responseDic["currency"]!)"
        let res = responseDic["earnings"] as! [[String: Any]]
        self.earningsMeta = res.map({EarningsMeta.init(responseDic: $0)})
    }
    
    
    struct EarningsMeta {
        let trips: String
        let day: String
        let total: Double
        let date: String
        
        
        struct SerializeableKeys {
            static let kEarningsTripsKey = "trips"
            static let kEarningsDayKey = "day"
            static let kEarningsTotalKey = "total"
            static let kEarningsDateKey = "date"
        }
        
        init(responseDic: [String: Any]) {
            self.trips = String(describing: responseDic[SerializeableKeys.kEarningsTripsKey]!)
            self.day = responseDic[SerializeableKeys.kEarningsDayKey] as! String
            self.total = Double(responseDic[SerializeableKeys.kEarningsTotalKey] as! Double)
            self.date = responseDic[SerializeableKeys.kEarningsDateKey] as! String
        }
        
    }
}
