//
//  userInputWindow.swift
//  Wakuk
//
//  Created by Jeya R on 5/9/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import UIKit

protocol userInputWindowDelegate
{
    func inputSubmitAction(userInput:String)
    func hidePopup()
}

class userInputWindow: UIView {
    
    @IBOutlet var UserInputView: UIView!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var txtInputField: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var delegate : userInputWindowDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("userInputWindow", owner: self, options: nil)
        self.addSubview(self.UserInputView!)
        initializeView()
        self.layoutIfNeeded()
    }
    
    func initializeView()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.UserInputView.addGestureRecognizer(tap)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height))
        self.txtInputField.leftView = paddingView
        self.txtInputField.leftViewMode = .always
        
        SetDoneToolbar(field: self.txtInputField)
        
     
    }
    
    func SetDoneToolbar(field:UITextField)
    {
        let doneToolbar:UIToolbar = UIToolbar()
        
        doneToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(keyboardCancel))
        ]
        
        doneToolbar.sizeToFit()
        field.inputAccessoryView = doneToolbar
    }
    
    @objc func keyboardCancel()
    {
        self.UserInputView.endEditing(true)
    }
    
    @objc func handleTap()
    {
//        keyboardCancel()
//        self.delegate?.hidePopup()
    }
    
    @IBAction func tapOnSubmit(_ sender: Any)
    {
        keyboardCancel()
        self.delegate?.inputSubmitAction(userInput: self.txtInputField.text!)
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}

extension userInputWindow : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    
    
}


