//
//  ChatViewController.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblChat: UITableView!
    
    @IBOutlet weak var lblOtherUserActivityStatus: UILabel!
    
    @IBOutlet weak var tvMessageEditor: UITextView!
    
    @IBOutlet weak var conBottomEditor: NSLayoutConstraint!
    
    @IBOutlet weak var lblNewsBanner: UILabel!
    
    
    
    var nickname: String! = ""
    
    var senderID : String!
    
    var chatMessages = [[String: AnyObject]]()
    
    var bannerLabelTimer: Timer!
    
    let dateFormatter = DateFormatter()
    let dateFormatter2 = DateFormatter()
    
    
    var chats:[ChatDetails] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        dateFormatter2.dateFormat = "MMM-dd HH:mm"
        nickname = "\(DATA().getUserId())"
        // Do any additional setup after loading the view.
        
        let realm = try! Realm()
        let chats = realm.objects(Chat.self).sorted(byKeyPath: "date", ascending: true)
        
        print("count \(chats.count)")
        
        
        
        for chat in chats {
            var msg = [String : AnyObject]()
            print(chat.message)
            msg["message"] = chat.message as AnyObject
            msg["request_id"] = chat.requestId as AnyObject
            msg["sender"] = chat.sender as AnyObject
            msg["time"] = chat.date as AnyObject
            //            chatMessages.append(msg)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.handleKeyboardDidShowNotification(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.handleKeyboardDidHideNotification(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.handleConnectedUserUpdateNotification(_:)), name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.handleDisconnectedUserUpdateNotification(_:)), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.handleUserTypingNotification(_:)), name: NSNotification.Name(rawValue: "userTypingNotification"), object: nil)
        
        
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ChatViewController.dismissKeyboard))
        swipeGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.down
        swipeGestureRecognizer.delegate = self
        view.addGestureRecognizer(swipeGestureRecognizer)
        
        
        //        SocketIOManager.sharedInstance.getChatMessage { (messageInfo) -> Void in
        //            DispatchQueue.main.async(execute: { () -> Void in
        //
        //                if let val = messageInfo["message"] as! [String: AnyObject]! {
        //                    if val.keys.contains("message") {
        //
        //                        let chat = Chat()
        //                        chat.message = val["message"] as! String
        //                        chat.requestId = val["request_id"] as! Int
        //
        //                        chat.sender = String(describing: val["sender"])
        //                        let timeString = val["time"] as! String
        //                        //self.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //                        let date = self.dateFormatter.date(from: timeString)!
        //                        chat.date = self.dateFormatter.string(from: date)
        //
        //                        let realm = try! Realm()
        //                        try! realm.write {
        //                            realm.add(chat, update: true)
        //                            //realm.add(chat)
        //                            print("realm write")
        //                        }
        //                        print("chat append")
        //                        self.chatMessages.append(val)
        //                        self.tblChat.reloadData()
        //                        self.scrollToBottom()
        //                    }
        //                }
        //            })
        //        }
        
        
        //        SocketIOManager.sharedInstance.connectToServerWithNickname(self.nickname, completionHandler: { (userList) -> Void in
        //            DispatchQueue.main.async(execute: { () -> Void in
        ////                if userList != nil {
        ////                    self.users = userList!
        ////                    self.tblUserList.reloadData()
        ////                    self.tblUserList.isHidden = false
        ////                }
        //                print("connected to server")
        //            })
        //        })
        //
        
        getMessages()
        startTimer()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableView()
        configureNewsBannerLabel()
        configureOtherUserActivityLabel()
        
        let defaults = UserDefaults.standard
        senderID = defaults.string(forKey: Const.Params.ID)
        
        
        tvMessageEditor.delegate = self
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        let realm = try! Realm()
        //        try! realm.write {
        //            realm.deleteAll()
        //        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    //MARK:- Timer Status
    var timer: DispatchSourceTimer?
    
    func startTimer() {
        let queue = DispatchQueue(label: "com.prov.nikola.timer")  // you can also use `DispatchQueue.main`, if you want
        timer = DispatchSource.makeTimerSource(queue: queue)
        timer!.scheduleRepeating(deadline: .now(), interval: .seconds(4))
        timer!.setEventHandler { [weak self] in
            // do whatever you want here
            
            self?.getMessages()
        }
        timer!.resume()
    }
    
    func stopTimer() {
        timer?.cancel()
        timer = nil
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        textView.text = nil
        textView.textColor = UIColor.black
        
        
        //        if textView.textColor == UIColor.lightGray {
        //            textView.text = nil
        //            textView.textColor = UIColor.black
        //        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
        SocketIOManager.sharedInstance.exitChatWithNickname(nickname) { () -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                //                self.nickname = nil
                //                self.users.removeAll()
                //                self.tblUserList.isHidden = true
                //                self.askForNickname()
            })
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: IBAction Methods
    @IBAction func backToTravelMap(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendMessage(_ sender: AnyObject) {
        if tvMessageEditor.text.count > 0 {
            
            //            var msg = [String: AnyObject]()
            //            msg["message"] = tvMessageEditor.text as AnyObject
            //            msg["request_id"] = DATA().getRequestId() as AnyObject
            //
            //
            //
            //
            //            msg["sender"] = "\(DATA().getUserId())" as AnyObject
            //
            //
            //            print(msg["sender"] ?? "sender id")
            //
            //
            //            let chat = Chat()
            //            chat.message = tvMessageEditor.text
            //            chat.requestId = DATA().getRequestId()
            //            chat.sender = String(format:"%f",DATA().USER_ID)
            //
            //             nickname = senderID
            //
            //
            //            //let timeString = val["time"] as! String
            //            //let date: Date = dateFormatter.date(from: timeString)!
            //            chat.date = self.dateFormatter.string(from: Date())
            //
            //            msg["time"] = chat.date as AnyObject
            //
            //            let realm = try! Realm()
            //            try! realm.write {
            //                realm.add(chat)
            //            }
            //
            //            SocketIOManager.sharedInstance.sendMessage(tvMessageEditor.text!, withNickname: nickname)
            self.updateMessages()
            
            tvMessageEditor.text = ""
            tvMessageEditor.resignFirstResponder()
            
            //self.chatMessages.append(msg)
            self.tblChat.reloadData()
            self.scrollToBottom()
        }
    }
    
    
    // MARK: Custom Methods
    
    func configureTableView() {
        tblChat.delegate = self
        tblChat.dataSource = self
        //        tblChat.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "idCellChat")
        
        tblChat.register(UINib(nibName: "ChatSendCell", bundle: nil), forCellReuseIdentifier: "idCellChat")
        
        tblChat.register(UINib(nibName: "ChatReceiveCell", bundle: nil), forCellReuseIdentifier: "chatReceive")
        tblChat.separatorStyle = .none
        
        tblChat.estimatedRowHeight = 90.0
        tblChat.rowHeight = UITableView.automaticDimension
        tblChat.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    func configureNewsBannerLabel() {
        lblNewsBanner.layer.cornerRadius = 15.0
        lblNewsBanner.clipsToBounds = true
        lblNewsBanner.alpha = 0.0
    }
    
    
    func configureOtherUserActivityLabel() {
        lblOtherUserActivityStatus.isHidden = true
        lblOtherUserActivityStatus.text = ""
    }
    
    
    @objc func handleKeyboardDidShowNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                conBottomEditor.constant = +250
                view.layoutIfNeeded()
            }
        }
    }
    
    
    @objc func handleKeyboardDidHideNotification(_ notification: Notification) {
        conBottomEditor.constant = 0
        view.layoutIfNeeded()
    }
    
    
    func scrollToBottom() {
        let delay = 0.1 * Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)) { () -> Void in
            if self.chats.count > 0 {
                let lastRowIndexPath = IndexPath(row: self.chats.count - 1, section: 0)
                self.tblChat.scrollToRow(at: lastRowIndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }
    }
    
    
    func showBannerLabelAnimated() {
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 1.0
            
        }, completion: { (finished) -> Void in
            self.bannerLabelTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ChatViewController.hideBannerLabel), userInfo: nil, repeats: false)
        })
    }
    
    
    @objc func hideBannerLabel() {
        if bannerLabelTimer != nil {
            bannerLabelTimer.invalidate()
            bannerLabelTimer = nil
        }
        
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 0.0
            
        }, completion: { (finished) -> Void in
        })
    }
    
    
    
    @objc func dismissKeyboard() {
        if tvMessageEditor.isFirstResponder {
            tvMessageEditor.resignFirstResponder()
            
            SocketIOManager.sharedInstance.sendStopTypingMessage(nickname)
        }
    }
    
    
    @objc func handleConnectedUserUpdateNotification(_ notification: Notification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        let connectedUserNickname = connectedUserInfo["nickname"] as? String
        lblNewsBanner.text = "User \(connectedUserNickname!.uppercased()) was just connected."
        showBannerLabelAnimated()
    }
    
    
    @objc func handleDisconnectedUserUpdateNotification(_ notification: Notification) {
        let disconnectedUserNickname = notification.object as! String
        lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased()) has left."
        showBannerLabelAnimated()
    }
    
    
    @objc func handleUserTypingNotification(_ notification: Notification) {
        if let typingUsersDictionary = notification.object as? [String: AnyObject] {
            var names = ""
            var totalTypingUsers = 0
            for (typingUser, _) in typingUsersDictionary {
                if typingUser != nickname {
                    names = (names == "") ? typingUser : "\(names), \(typingUser)"
                    totalTypingUsers += 1
                }
            }
            
            if totalTypingUsers > 0 {
                let verb = (totalTypingUsers == 1) ? "is" : "are"
                
                lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
                lblOtherUserActivityStatus.isHidden = false
            }
            else {
                lblOtherUserActivityStatus.isHidden = true
            }
        }
        
    }
    
    
    // MARK: UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chats.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idCellChat", for: indexPath) as! ChatCell
        
        //        let currentChatMessage = chatMessages[indexPath.row]
        //print(currentChatMessage)
        //let senderNickname = currentChatMessage["nickname"] as! String
        //let message = currentChatMessage["message"] as! String//as! [String: AnyObject]
        //let messageDate = currentChatMessage["date"] as! String
        
        //        print(currentChatMessage["sender"] as! String)
        
        
        //          let send : String = currentChatMessage["sender"] as! String
        
        
        //
        //        if let stringArray = currentChatMessage["sender"] as? String {
        //            // obj is a string array. Do something with stringArray
        //            let send : String = currentChatMessage["sender"] as! String
        //
        //            let senderNickname = send
        //
        //            if senderNickname == nickname {
        //
        //
        //                let cell = tableView.dequeueReusableCell(withIdentifier: "idCellChat", for: indexPath) as! ChatCell
        //
        //
        //                cell.lblChatMessage.textAlignment = NSTextAlignment.right
        //                cell.lblMessageDetails.textAlignment = NSTextAlignment.right
        //
        //                cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
        //
        //                //            let date = self.dateFormatter.date(from: currentChatMessage["time"] as! String)!
        //                //            let newDate = self.dateFormatter2.string(from: date)
        //
        //                cell.lblChatMessage.text = "\(message)" // + "\(message["sender"] as! String)"
        //                //cell.lblMessageDetails.text = "by \(senderNickname.uppercased()) @ \(messageDate)"
        //                //            cell.lblMessageDetails.text = "@ \(newDate)"
        //
        //                cell.lblChatMessage.textColor = UIColor.white
        //
        //                return cell
        //
        //
        //
        //            }
        //            else {
        //
        //                let cell = tableView.dequeueReusableCell(withIdentifier: "chatReceive", for: indexPath) as! ChatCell
        //
        //
        //                cell.lblChatMessage.textAlignment = NSTextAlignment.left
        //                //            cell.lblMessageDetails.textAlignment = NSTextAlignment.left
        //                cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
        //
        //
        //                cell.lblChatMessage.textAlignment = NSTextAlignment.right
        //                //            cell.lblMessageDetails.textAlignment = NSTextAlignment.right
        //
        //                cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
        //
        //                //            let date = self.dateFormatter.date(from: currentChatMessage["time"] as! String)!
        //                //            let newDate = self.dateFormatter2.string(from: date)
        //
        //                cell.lblChatMessage.text = "\(message)" // + "\(message["sender"] as! String)"
        //                //cell.lblMessageDetails.text = "by \(senderNickname.uppercased()) @ \(messageDate)"
        //                //            cell.lblMessageDetails.text = "@ \(newDate)"
        //
        //                cell.lblChatMessage.textColor = UIColor.black
        //
        //
        //
        //
        //                return cell
        //
        //
        //
        //            }
        //
        //
        //
        //
        //        }
        //
        //        else {
        //            let ss : Int =  currentChatMessage["sender"] as! Int
        //
        //            print(ss)
        //
        //
        //            let send : String = String(ss)
        //
        //
        //            print(send)
        //
        //
        //            let senderNickname = send
        let is_user = self.chats[indexPath.row].is_user
        
        let message = self.chats[indexPath.row].message
        
        
        print(send)
        
        //let senderNickname = send
        
        if is_user == "0" {
            //            if senderNickname == nickname {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "idCellChat", for: indexPath) as! ChatCell
            
            
            cell.lblChatMessage.textAlignment = NSTextAlignment.right
            cell.lblMessageDetails.textAlignment = NSTextAlignment.right
            
            cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
            
            //            let date = self.dateFormatter.date(from: currentChatMessage["time"] as! String)!
            //            let newDate = self.dateFormatter2.string(from: date)
            
            cell.lblChatMessage.text = "\(message)" // + "\(message["sender"] as! String)"
            //cell.lblMessageDetails.text = "by \(senderNickname.uppercased()) @ \(messageDate)"
            //            cell.lblMessageDetails.text = "@ \(newDate)"
            
            cell.lblChatMessage.textColor = UIColor.white
            
            return cell
            
            
            
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "chatReceive", for: indexPath) as! ChatCell
            
            
            cell.lblChatMessage.textAlignment = NSTextAlignment.left
            //            cell.lblMessageDetails.textAlignment = NSTextAlignment.left
            cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
            
            
            cell.lblChatMessage.textAlignment = NSTextAlignment.right
            //            cell.lblMessageDetails.textAlignment = NSTextAlignment.right
            
            cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
            
            //            let date = self.dateFormatter.date(from: currentChatMessage["time"] as! String)!
            //            let newDate = self.dateFormatter2.string(from: date)
            
            cell.lblChatMessage.text = "\(message)" // + "\(message["sender"] as! String)"
            //cell.lblMessageDetails.text = "by \(senderNickname.uppercased()) @ \(messageDate)"
            //            cell.lblMessageDetails.text = "@ \(newDate)"
            
            cell.lblChatMessage.textColor = UIColor.black
            
            
            
            
            return cell
            
            
            
        }
        
        
        // }
        
        
        
        
        //        if senderNickname == nickname {
        //            cell.lblChatMessage.textAlignment = NSTextAlignment.right
        //            cell.lblMessageDetails.textAlignment = NSTextAlignment.right
        //
        //            cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
        //        }else{
        //            cell.lblChatMessage.textAlignment = NSTextAlignment.left
        //            cell.lblMessageDetails.textAlignment = NSTextAlignment.left
        //            cell.lblChatMessage.textColor = lblNewsBanner.backgroundColor
        //        }
        //
        //        let date = self.dateFormatter.date(from: currentChatMessage["time"] as! String)!
        //        let newDate = self.dateFormatter2.string(from: date)
        //
        //        cell.lblChatMessage.text = "\(message)" // + "\(message["sender"] as! String)"
        //        //cell.lblMessageDetails.text = "by \(senderNickname.uppercased()) @ \(messageDate)"
        //        cell.lblMessageDetails.text = "@ \(newDate)"
        //
        //        cell.lblChatMessage.textColor = UIColor.darkGray
        
        //  return cell
    }
    
    
    // MARK: UITextViewDelegate Methods
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        SocketIOManager.sharedInstance.sendStartTypingMessage(nickname)
        
        return true
    }
    
    
    // MARK: UIGestureRecognizerDelegate Methods
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func getMessages(){
        
        API.getMessage(completionHandler:{ json, error in
            
            if let error = error {
                //self.hideLoader()
                //self.showAlert(with :error.localizedDescription)
                debugPrint("Error occuring while fetching provider.service :( | \(error.localizedDescription)")
            }else {
                
                if let json = json {
                    
                    let status = json[Const.STATUS_CODE].boolValue
                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                    if(status){
                        
                        print(json ?? "error in sendPay json")
                        let chatsArray = json["data"].arrayValue
                        self.chats.removeAll()
                        for type: JSON in chatsArray {
                            self.chats.append(ChatDetails.init(jsonObj: type))
                        }
                        self.tblChat.reloadData()
                    }else{
                        // print(statusMessage)
                        print(json ?? "sendPay json empty")
                        //var msg = json![Const.DATA].rawString()!
                        //                msg = msg.replacingOccurrences( of:"[{}\",]", with: "", options: .regularExpression)
                        
                        
                        //                        if let errorCode : Int = json["error_code"].int {
                        //                            if errorCode == 150 {
                        //                                self.view.makeToast(message: "Waiting for driver to confirm the payment")
                        //                            }
                        //                        }
                        
                        // let errorCode: Int = json["error_code"].int!
                        
                        
                        
                    }
                    
                }else{
                    //self.hideLoader()
                    debugPrint("Invalid JSON :(")
                }
                
            }
            
            
        })
    }
    
    
    func messageNotification(){
        
        API.messageNotification(completionHandler:{ json, error in
            
            if let error = error {
                //self.hideLoader()
                //self.showAlert(with :error.localizedDescription)
                debugPrint("Error occuring while fetching provider.service :( | \(error.localizedDescription)")
            }else {
                
                if let json = json {
                    
                    let status = json[Const.STATUS_CODE].boolValue
                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                    if(status){
                        
                        print(json ?? "error in sendPay json")
                        
                    }else{
                        // print(statusMessage)
                        print(json ?? "sendPay json empty")
                        
                        
                        
                    }
                    
                }else{
                    //self.hideLoader()
                    debugPrint("Invalid JSON :(")
                }
                
            }
            
            
        })
    }
    
    func updateMessages(){
        
        API.updateMessage(message: tvMessageEditor.text,completionHandler: { json, error in
            
            if let error = error {
                //self.hideLoader()
                //self.showAlert(with :error.localizedDescription)
                debugPrint("Error occuring while fetching provider.service :( | \(error.localizedDescription)")
            }else {
                
                if let json = json {
                    
                    let status = json[Const.STATUS_CODE].boolValue
                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                    if(status){
                        
                        print(json ?? "error in sendPay json")
                        self.messageNotification()
                        self.getMessages()
                    }else{
                        // print(statusMessage)
                        print(json ?? "sendPay json empty")
                        //var msg = json![Const.DATA].rawString()!
                        //                msg = msg.replacingOccurrences( of:"[{}\",]", with: "", options: .regularExpression)
                        
                        
                        //                        if let errorCode : Int = json["error_code"].int {
                        //                            if errorCode == 150 {
                        //                                self.view.makeToast(message: "Waiting for driver to confirm the payment")
                        //                            }
                        //                        }
                        
                        // let errorCode: Int = json["error_code"].int!
                        
                        
                        
                    }
                    
                }else{
                    //self.hideLoader()
                    debugPrint("Invalid JSON :(")
                }
                
            }
            
            
        })
    }
    
    
}

class Chat: Object {
    dynamic var message = ""
    dynamic var sender = ""
    dynamic var date = ""
    dynamic var requestId = 0
    
    override static func primaryKey() -> String? {
        return "date"
    }

}
