//
//  Chat.swift
//  Grid
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Nikola. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatDetails {
    
    var request_id: String = ""
    var message: String = "", id: String = "", is_user: String = "", updated_at: String = "", created_at: String = ""
    
    var jsnObj: JSON!
    
    init(jsonObj: JSON) {
        request_id = jsonObj["request_id"].stringValue
        message = jsonObj["message"].stringValue
        id = "\(jsonObj["id"])"
        is_user = "\(jsonObj["is_user"])"
        updated_at = jsonObj["updated_at"].stringValue
        created_at = jsonObj["created_at"].stringValue
        
        jsnObj = jsonObj
    }
    
}

