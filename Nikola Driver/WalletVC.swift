//
//  WalletVC.swift
//  Nikola
//
//  Created by Shantha Kumar on 9/29/17.
//  Copyright © 2017 Sutharshan. All rights reserved.
//

import UIKit
//import Stripe

protocol paymentconfimationprotocaldelegate
{
    
    func paymentconfimationmethod()
    
}
protocol stripepaymentshowdelegate
{
    func stripepaymentVCShow()
}

class WalletVC: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var burgerMenu: UIBarButtonItem!
    
    @IBOutlet weak var lblWalletMoney: UILabel!
    
    @IBOutlet weak var PaymentTypeSelection: PaymentTypeSelection!
    
    var currency = ""
    
    var isBackBoolVal : Bool?
    
    @IBOutlet weak var userInputView: userInputWindow?
    
    @IBOutlet weak var bgView: UIView!
    
    var userInputURL:String = ""
    var userInputReferenceId:String = ""
    var userInputAuthItem:String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let currn = UserDefaults.standard.string(forKey: "currency") {
            currency = currn
        }
        self.PaymentTypeSelection?.delegate = self
        self.userInputView?.delegate = self
        
        if revealViewController() != nil
        {
            self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        }
        
        if let boolValue = isBackBoolVal
        {
            if boolValue == true
            {
                
                burgerMenu.image = UIImage(named: "blackBackIcon")
            }
            else {
                burgerMenu.image = UIImage(named: "Burger")
                
            }
            
        }
        else {
            burgerMenu.image = UIImage(named: "Burger")
        }
        
        txtAmount.delegate = self
        
        txtAmount.layer.masksToBounds = false
        txtAmount.layer.shadowRadius = 1.0
        txtAmount.layer.shadowColor = UIColor.black.cgColor
        txtAmount.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        txtAmount.layer.shadowOpacity = 1.0
        
        
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(WalletVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(WalletVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getBalance()
        self.userInputView?.isHidden = true
        self.PaymentTypeSelection?.isHidden = true
        self.txtAmount.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.bgView.frame.origin.y == 0{
                self.bgView.frame.origin.y -= 50
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.bgView.frame.origin.y != 0{
                self.bgView.frame.origin.y += 50
            }
        }
    }
    
    func backButton(sender: Any) {
        
        
        revealViewController().revealToggle(sender)
        
        
        
    }
    
    func backNewMethod(sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func getBalance() {
        self.showLoader(str: "Fetching your balance")
        
        API.refreshWalletBalance(completionHandler: { json, error in
            
            if json == nil
            {
                self.hideLoader()
                return
            }
                
                
            else {
                let json = self.jsonParser.jsonParser(dicData: json)
                
                let status = json[Const.STATUS_CODE].boolValue
                
                
                print(json)
                
                //                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    let amount = json["amount"].stringValue
                    self.lblWalletMoney.text = "NGN " + "\(amount)"
                    
                    // let data  = json["amount"].dictionary
                    
                    //                    let user_Balance : String = (json!["amount"]?.string)!
                    //
                    //                    self.lblWalletMoney.text = "$ \(user_Balance)"
                    self.hideLoader()
                    
                }
                else {
                    self.hideLoader()
                    var msg = json["text"].rawString()!
                    
                    self.showAlert(with: msg)
                }
                
                
            }
            
        })
        
        //        API.walletBalance() { responseObject, error in
        //
        //            if responseObject == nil
        //            {
        //                self.hideLoader()
        //                return
        //            }
        //            else {
        //                let json = self.jsonParser.jsonParser(dicData: responseObject)
        //
        //                let status = json[Const.STATUS_CODE].boolValue
        //                print(json)
        //
        //                //                    let statusMessage = json[Const.STATUS_MESSAGE].stringValue
        //                if(status){
        //
        //                    let data  = json["data"].dictionary
        //
        //                    let user_Balance : String = (data!["user_balance"]?.string)!
        //
        //                    self.lblWalletMoney.text = "$ \(user_Balance)"
        //                    self.hideLoader()
        //
        //                }
        //                else {
        //                    self.hideLoader()
        //                    var msg = json["text"].rawString()!
        //
        //                    self.showAlert(with: msg)
        //                }
        //
        //
        //            }
        //
        //
        //
        //
        //
        //
        //        }
        
        
    }
    
    @IBAction func firstMoneyButtonAction(_ sender: Any) {
        
        txtAmount.text = "  1000"
        
    }
    
    
    @IBAction func secondMoneyButtonAction(_ sender: Any) {
        
        txtAmount.text = "  2000"
        
    }
    
    
    @IBAction func thirdMoneyButtionAction(_ sender: Any) {
        
        txtAmount.text = "  3000"
        
    }
    
    @IBAction func BackActionMethod(_ sender: Any) {
        
        if let boolValue = isBackBoolVal {
            if boolValue == true {
                self.dismiss(animated: true, completion: nil)
            }else {
                revealViewController().revealToggle(sender)
            }
            
        }else {
            revealViewController().revealToggle(sender)
        }
        
    }
    
    @IBAction func addAmountButtonAction(_ sender: Any)
    {
        
        if (txtAmount.text == "")
        {
            self.showAlert(with: "Enter Amount")
        }
        else
        {
            self.PaymentTypeSelection.isHidden = false
            
           // self.payByCardAction1()
            
            
        }
        
        
        
        
        
        
        //      if (txtAmount.text?.isEmpty)! {
        //
        //            print("empty")
        //
        //        }
        //        else {
        //
        //            self.showLoader(str: "Please wait")
        //
        //            API.walletPaymentgateType() { [weak self] responseObject, error in
        //
        //                if responseObject == nil {
        //                    return
        //                }
        //                else {
        //                        let json = self?.jsonParser.jsonParser(dicData: responseObject)
        //                if let json = json {
        //                        let status = json[Const.STATUS_CODE].boolValue
        //                        let statusMessage = json[Const.STATUS_MESSAGE].stringValue
        //                        if(status){
        //                            print(json)
        //                            let str : String = (self?.txtAmount.text!)!
        //                            let trimmedString = str.trimmingCharacters(in: .whitespaces)
        //
        //                            let val : Int = Int(trimmedString)!
        //
        //                            print(val)
        //
        //                            self?.hideLoader()
        //                            let gateways = json["data"].dictionary
        //                            guard let array = (gateways!["gateways"]?.array), array.count > 0 else {
        //                                self?.showAlert(with: "No Wallet Gateway found! Please Configure Payment Gateway from dashboard.")
        //                                return
        //                            }
        //
        //
        //                            let popOverVC: WalletPaymentGateType   = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WalletPaymentGateTypeVC") as? WalletPaymentGateType)!
        //
        //                            popOverVC.walletPaymentGatewayTypeArray = responseObject
        //                            popOverVC.walletMoney = val
        //                            popOverVC.delegate = self
        //                            popOverVC.delegatestripe = self
        //                            popOverVC.view.frame = CGRect(x: 0, y: 0, width: (self?.view.frame.width)!, height: (self?.view.frame.height)! + 70)
        //
        //
        //                            //
        //                            popOverVC.willMove(toParentViewController: self)
        //                            self?.view.addSubview(popOverVC.view)
        //                            self?.addChildViewController(popOverVC)
        //                            popOverVC.didMove(toParentViewController: self)
        //
        //                            // UIApplication.shared.keyWindow?.addSubview(popOverVC.view)
        //
        //                        }
        //                        else {
        //
        //                            print(statusMessage)
        //
        //
        //                            self?.hideLoader()
        //
        //                            if let msg = json["text"].rawString() {
        //                                self?.showAlert(with: msg)
        //                            }
        //
        //
        //                        }
        //                    }
        //
        //
        //
        //                }
        //
        //
        //
        //
        //            }
        //
        //        }
        //        print("no empty")
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}

extension WalletVC : stripepaymentshowdelegate {
    
    func stripepaymentVCShow() {}
}

extension WalletVC : paymentconfimationprotocaldelegate {
    
    func paymentconfimationmethod() {
        
        if let boolValue = isBackBoolVal {
            if boolValue == true {
                self.dismiss(animated: true, completion: nil)
            }else {
                self.getBalance()
            }
        }else {
            self.getBalance()
        }
        
    }
    
    func payByCardAction1()
    {
        self.showLoader(str: "Payment in Progress...")
        
        API.addAmountToWallet(paymentMode: "card", amount: txtAmount.text!,bankCode:"", bankAccNumber:"",  completionHandler: { json, error in
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    
                    self.PaymentTypeSelection?.isHidden = true
                    
                    if json["status"].string == Const.STATUS_CODE
                    {
                        self.view.makeToast(message: "Amount added to wallet")
                        self.getBalance()
                        return
                    }
                    else if json["status"].string == Const.STATUS_FAILED
                    {
                        self.view.makeToast(message: "Payment Failed! Please Try again")
                        return
                    }
                    else
                    {
                        
                    }
                    
                    
                    let compareStr = json[Const.STATUS].string
                    self.userInputURL = json[Const.URL_POST_DATA].string!
                    self.userInputReferenceId = json[Const.Params.REFERENCE_ID].string!
                    
                    print(self.userInputURL)
                    print(self.userInputReferenceId)
                    
                    self.userInputView?.txtInputField.text = ""
                    
                    
                    self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numberPad
                    
                    self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numberPad
                    
                    if compareStr == "send_pin"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter PIN"
                    }
                    else if compareStr == "send_phone"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter Phone number"
                    }
                    else if compareStr == "send_otp"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter OTP"
                    }
                    else if compareStr == "send_birthday"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numbersAndPunctuation
                        self.userInputView?.txtInputField.placeholder = "Enter Birthday - YYYY-MM-DD"
                    }
                    else
                    {
                        self.PaymentTypeSelection.isHidden = true
                        self.view.makeToast(message: statusMessage)
                    }
                    
                    print(json ?? "error in sendNonce json")
                }
                else
                {
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                   self.PaymentTypeSelection.isHidden = true
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                        self.view.makeToast(message: msg)
                    }
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
            
        })
    }
    
}

extension WalletVC: PaymentTypeSelectionDelegate
{
    func payByCardAction()
    {
        self.showLoader(str: "Payment in Progress...")
        
        API.addAmountToWallet(paymentMode: "card", amount: txtAmount.text!,bankCode:"", bankAccNumber:"",  completionHandler: { json, error in
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    
                    self.PaymentTypeSelection?.isHidden = true
                    
                    if json["status"].string == Const.STATUS_CODE
                    {
                        self.view.makeToast(message: "Amount added to wallet")
                        self.getBalance()
                        return
                    }
                    else if json["status"].string == Const.STATUS_FAILED
                    {
                        self.view.makeToast(message: "Payment Failed! Please Try again")
                        return
                    }
                    else
                    {
                        
                    }
                    
                    
                    let compareStr = json[Const.STATUS].string
                    self.userInputURL = json[Const.URL_POST_DATA].string!
                    self.userInputReferenceId = json[Const.Params.REFERENCE_ID].string!
                    
                    print(self.userInputURL)
                    print(self.userInputReferenceId)
                    
                    self.userInputView?.txtInputField.text = ""
                    
                    
                    self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numberPad
                    
                    self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numberPad
                    
                    if compareStr == "send_pin"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter PIN"
                    }
                    else if compareStr == "send_phone"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter Phone number"
                    }
                    else if compareStr == "send_otp"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter OTP"
                    }
                    else if compareStr == "send_birthday"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numbersAndPunctuation
                        self.userInputView?.txtInputField.placeholder = "Enter Birthday - YYYY-MM-DD"
                    }
                    else
                    {
                        self.PaymentTypeSelection.isHidden = true
                        self.view.makeToast(message: statusMessage)
                    }
                    
                    print(json ?? "error in sendNonce json")
                }
                else
                {
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                    self.PaymentTypeSelection.isHidden = true
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                        self.view.makeToast(message: msg)
                    }
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
            
        })
    }
    
    func payByBankAction()
    {
        self.performSegue(withIdentifier: "goToPayByBank", sender: self)
    }
    
    func cancelPopup()
    {
        self.PaymentTypeSelection.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "goToPayByBank")
        {
            let vc = segue.destination as! PayByBankViewController
            vc.walletAmount = self.txtAmount.text!
        }
    }
    
}

extension WalletVC: userInputWindowDelegate
{
    
    func hidePopup()
    {
        self.userInputView?.isHidden = true
    }
    
    func inputSubmitAction(userInput:String)
    {
        
        if (userInput == "")
        {
            self.showAlert(with: "All Fields are mandatory")
            return
        }
        
        
        self.showLoader(str: "Verify your input...")
        
        API.verifyUserInput(inputURL: self.userInputURL, referenceId: self.userInputReferenceId, authInput: userInput, payBy: "wallet", completionHandler: { json, error in
            
            
            self.hideLoader()
            
            if (error != nil)
            {
                print(error.debugDescription)
                self.view.makeToast(message: (error?.localizedDescription)!)
                return
            }
            
            if let json = json
            {
                
                
                
                let status = json[Const.STATUS_CODE].boolValue
                let statusMessage = json[Const.STATUS_MESSAGE].stringValue
                if(status)
                {
                    
                    self.PaymentTypeSelection.isHidden = true
                    
                    if json["status"].string == Const.STATUS_CODE
                    {
                        self.view.makeToast(message: "Amount added to wallet")
                        self.getBalance()
                        self.userInputView?.isHidden = true
                        return
                    }
                    else if json["status"].string == Const.STATUS_FAILED
                    {
                        self.view.makeToast(message: "Payment Failed! Please Try again")
                        return
                    }
                    else
                    {
                        
                    }
                    
                    let compareStr = json[Const.STATUS].string
                    self.userInputURL = json[Const.URL_POST_DATA].string!
                    self.userInputReferenceId = json[Const.Params.REFERENCE_ID].string!
                    
                    print(self.userInputURL)
                    print(self.userInputReferenceId)
                    
                    self.userInputView?.txtInputField.text = ""
                    self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numberPad
                    
                    
                    if compareStr == "send_pin"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter PIN"
                    }
                    else if compareStr == "send_phone"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter Phone number"
                    }
                    else if compareStr == "send_otp"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.placeholder = "Enter OTP"
                    }
                    else if compareStr == "send_birthday"
                    {
                        self.userInputView?.isHidden = false
                        self.userInputView?.txtInputField.keyboardType = UIKeyboardType.numbersAndPunctuation
                        
                        self.userInputView?.txtInputField.placeholder = "YEnter Birthday - YYYY-MM-DD"
                    }
                    else
                    {
                        self.PaymentTypeSelection.isHidden = true
                        self.view.makeToast(message: statusMessage)
                    }
                    
                    print(json ?? "error in sendNonce json")
                }
                else
                {
                    print(statusMessage)
                    print(json ?? "sendNonce json empty")
                    self.PaymentTypeSelection.isHidden = true
                    
                    if let msg : String = json[Const.ERROR].rawString() {
                        self.view.makeToast(message: msg)
                    }
                    
                }
                
            }else
            {
                debugPrint("Invalid Json :(")
            }
            
        })
        
    }
}

